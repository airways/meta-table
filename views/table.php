
<table width="100%" cellpadding="0" cellspacing="0" class="meta_table">
    <tr>
        <td align="left" valign="top">
        
            <input type="hidden" id="<?= $table_id ?>_eid" name="<?= $field_name ?>[eid]" value="<?= $table_id ?>" />
            <input type="hidden" id="<?= $table_id ?>_cols" name="<?= $field_name ?>[cols]" value="<?= $cols ?>" />
            <input type="hidden" id="<?= $table_id ?>_rows" name="<?= $field_name ?>[rows]" value="<?= $rows ?>" />
            
            <table border="0" class="element_meta_table" cellpadding="0" cellspacing="0" width="100%" id="<?= $table_id ?>" rel="<?= $field_name ?>" >
            
            <?php $cell_index = 0; ?>
            <?php if ($header): ?>
            
            <thead>
            
                <td class="line_number">&nbsp;</td> 
            
                <?php for ($j=0; $j<$cols; $j++): ?>
                
                <td>
                    <div class="element_meta_table_td_wrapper">
                        <input type="text" class="element_box" name="<?= $field_name ?>[cell][]" value="<?= htmlspecialchars($col_settings[$j]->label) ?>" disabled="disabled">
                    </div>
                </td>
                
                <?php endfor; ?>
                
                <td width="20"></td>
            </thead>
            <tbody>
            
            <?php endif;?>
            
                        
            <?php for ($i=0; $i<$rows+1; $i++): ?>        
                <?php if($i > $rows-1) echo '</tbody><tfoot>'; ?>
                <tr class="<?php echo ($i > $rows-1 ? 'template' : '')?>">
                    <td class="line_number"></td> 
                    
                        <?php for ($j=0; $j<$cols; $j++): ?>
                        
                            <td>
                                <div class="element_meta_table_td_wrapper <?php echo htmlspecialchars($col_settings[$j]->type); ?>_field">
                                    <?php
                                    switch($col_settings[$j]->type)
                                    {
                                        case 'text': ?>
                                            <input type="text" class="element_box" name="<?= $field_name ?>[cell][]" value="<?= htmlspecialchars(@$cell[$cell_index++]) ?>">
                                            <?php
                                            break;
                                        case 'list':
                                            echo str_replace('name=', 'class="element_box" name=', form_dropdown($field_name.'[cell][]', $col_settings[$j]->options, $cell[$cell_index++]));
                                            break;
                                        case 'relationship': ?>
                                            <input type="text" class="element_box" name="<?= $field_name ?>[cell][]" value="<?= htmlspecialchars(@$cell[$cell_index++]) ?>" i
                                                data-source="<?= $col_settings[$j]->source ?>"><img class="autocomplete_handle" 
                                                src="<?php echo ee()->config->item('theme_folder_url'); ?>/cp_themes/nerdery/images/field_expand.png" />
                                            <?php
                                            break;
                                        case 'file':
                                            $val = @$cell[$cell_index++];
                                            $arr = explode('|', $val);
                                            if(count($arr) < 2) $arr[] = '';
                                            if(count($arr) < 3) $arr[] = '';
                                            if($arr[0] && $arr[1]) {
                                                ee()->load->library('filemanager');
                                                $thumb = ee()->filemanager->get_thumb($arr[1], $arr[2], FALSE);
                                            } else {
                                                $thumb = FALSE;
                                            }
                                            ?>
                                             <div class="element_box">
                                             <input type="hidden" name="<?= $field_name ?>[cell][]" value="<?= htmlspecialchars($val) ?>">
                                             <a href="#" class="choose_file">Select file...</a>
                                             <span class="meta_filename"><?= htmlspecialchars($arr[1]) ?></span>
                                             <div class="file_thumb" <?php if(!$thumb) echo 'style="display: none;"'; ?>>
                                                 <img src="<?php echo $thumb['thumb']; ?>" />
                                                 <a href="#" class="remove_file">Remove file</a>
                                             </div>
                                             </div>
                                            <?php
                                            break;
                                        default:
                                            echo 'Unknown type: '.htmlspecialchars($col_settings[$j]->type);
                                    }
                                    ?>
                                </div>
                            </td>   
                        
                        <?php endfor; ?>
                        
                        <!-- drag handle added to this empty cell-->
                        <td width="20" class="drag_cell"><div class="element_meta_table_td_wrapper"></div></td>
                </tr>       
                <?php if($i > $rows) echo '</tfoot>'; ?>

            <?php endfor; ?>

            </table>

        </td>
    </tr>
</table>
        
<div class="element_meta_table_buttons_row">
    <a href="#" rel="<?= $table_id ?>" class="element_meta_table_button element_meta_table_add_row" style="color: #E11842"><?= lang('add_row') ?></a>
    <a href="#" rel="<?= $table_id ?>" class="element_meta_table_button element_meta_table_remove_row" style="color: #E11842"><?= lang('remove_row') ?></a>

    <script type="text/javascript">try { apply_table_sortable_script(); } catch(e) {} </script>
</div>

<div class="content_elements_clr"></div>
