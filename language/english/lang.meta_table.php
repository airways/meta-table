<?php

$lang = array(

'meta_table_element_name'                    => 'Meta Table',

'add_row'   => "Add Row",
'remove_row'    => "Remove Row",

'table_init_rows'   => 'Starting Rows',
'table_init_cols'   => 'Cols',
'table_header'      => 'Include Table Header',
'table_column_settings' => 'Column Settings',
'table_column_settings_help' => <<<END
<br/>
<b>Help</b><br/>
Each line should contain settings for an individual column in this format:<br/>
<pre>
col_name ; Column Label ; type
</pre>

Type is optional and may be <b>text</b> (the default), <b>list</b> or <b>relationship</b>. If the type is <b>list</b>, an additional
value containing the options and their labels is also required:

<pre>
example_list ; Example List ; list ; option_1:Option 1 | option_2:Option 2
</pre>

Relationships also take parameters using the same option syntax:

<pre>
name ; label ; relationship ; channel:Pages | category:55,56
</pre>

END
,

"" => ""

); 