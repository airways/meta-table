<?php

// @version 1.02
// @element
// @dir meta_table

define('META_TABLE_VERSION', '1.02');
define('META_TABLE_NAME', 'Meta Table');
define('META_TABLE_CLASS', 'Meta_table'); // must match module class name
define('META_TABLE_DESCRIPTION', 'Meta table provides a multi-row, typed-column field type for Content Elements'); 
define('META_TABLE_DOCSURL', 'http://metasushi.com/');
define('META_TABLE_DEBUG', TRUE);
