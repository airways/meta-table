﻿ContentElements.bind('meta_table', 'display', function(data)
{
	elm_hash_key = data.data('el-id');
	
	if(typeof elm_hash_key == 'undefined')
		elm_hash_key = ContentElements.randomString();

	data.html(data.html().replace(/\__element_meta_table_index__/g, elm_hash_key));
	data.find('input[type="text"]:first').focus();
	
	ceMetaTable.apply_table_refresh_row_numbers();
	ceMetaTable.apply_table_sortable_script();
	ceMetaTable.apply_buttons(data);	

	metaTableRelationship.bind(data)
	metaTableFile.bind(data);
});


/*$(document).ready(function() {
	function setupFileField(container) {
		var last_value = [],
			fileselector = container.find('.no_file'),
			hidden_name = container.find('input[name*="_hidden_file"]').prop('name'),
			placeholder;

		if ( ! hidden_name) {
			return;
		}

		remove = $('<input/>', {
			'type': 'hidden',
			'value': '',
			'name': hidden_name.replace('_hidden_file', '')
		});

		container.find(".remove_file").click(function() {
			container.find("input[type=hidden][name*='hidden']").val(function(i, current_value) {
				last_value[i] = current_value;
				return '';
			});
			container.find(".file_set").hide();
			container.find('.sub_filename a').show();
			fileselector.show();
			container.append(remove);

			return false;
		});

		container.find('.undo_remove').click(function() {
			container.find("input[type=hidden]").val(function(i) {
				return last_value.length ? last_value[i] : '';
			});
			container.find(".file_set").show();
			container.find('.sub_filename a').hide();
			fileselector.hide();
			remove.remove();

			return false;
		});
	}
	console.log($('.element_meta_table .file_field'));
	$('.element_meta_table .file_field').each(function() {
		setupFileField($(this));
	});

});*/

