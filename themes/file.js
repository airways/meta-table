var metaTableFile = {
    bind: function(data) {
        $.ee_filebrowser.add_trigger('.element_meta_table_td_wrapper .choose_file', function (file) {
           $(this).parents('.file_field').find('input').val(file.file_id+'|'+file.title+'|'+file.upload_location_id);
           $(this).parents('.file_field').find('.meta_filename').html(file.title);
           $(this).parents('.file_field').find('img').attr('src', file.thumb);
           $(this).parents('.file_field').find('.file_thumb').css('display', 'block');
        });
        $(data).find('.element_meta_table_td_wrapper .remove_file').unbind('click').click(function() {
            $(this).parents('.file_field').find('input').val('');
            $(this).parents('.file_field').find('.meta_filename').html('');
            $(this).parents('.file_field').find('.file_thumb').css('display', 'none');
            return false;
        });
    }
}

$(document).ready(function() {
    metaTableFile.bind($('body'));
});
