var metaTableRelationship = {
    bind: function(data) {
        $(data).find('.relationship_field input').each(function() {
            var $this = $(this);
            var source = $this.attr('data-source');
            $this.autocomplete({source: metaTableSitePages})
            .removeClass('ui-autocomplete-input')
            .click(function() { 
                $('.relationship_field').find('input').autocomplete('close');
                if($(this).val() == '') {
                    $(this).val('/');
                } else {
                    var val = $(this).val();
                    $(this).val('');
                    $(this).val(val);
                }
                $(this).autocomplete('search');
            });
        });

        $(data).find('.relationship_field .autocomplete_handle').click(function() {
            $(this).parents('.relationship_field').find('input').trigger('click');
        });
    }
    
    
};

$(document).ready(function() {
    metaTableRelationship.bind('.meta_table');
});
