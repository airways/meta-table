<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Meta_table_element Class
 *
 * @package     Content_elements
 * @author      KREA SK s.r.o.
 * @author      Isaac Raway / @airways / isaac@meta.mn
 * @copyright   Copyright (c) 2012, KREA SK s.r.o.
 * @link        http://www.krea.com
 */
class Meta_table_element {

    var $info = array(
        'name' => 'Meta Table',
    );
    var $settings = array();
    var $cache = array();
    var $FILE_ID = 0;
    var $FILE_NAME = 1;
    var $FILE_LOC_ID = 2;

    //CONSTRUCT 

    function __construct() {
        $this->EE = &get_instance();
    }

    /**
     * Get tile html: ELEMENT HTML (BACKEND)
     *
     * @param int field_id
     * @param mixed element value
     * @param array settings
     * @param array all saved data together
     * @return string html
     */
    function display_element($data) { 
        $assets = '';
        
        $this->EE->load->model('file_model');
        $this->EE->load->library('file_field');
        
        // first time... load css & js
        if (!isset($this->cache['assets_loaded'])) {
            $theme_url = rtrim(CE_THEME_URL, '/') . '/elements/meta_table/';

            /*
              $this->EE->cp->add_to_head('<link rel="stylesheet" href="' . $theme_url . 'styles.css" type="text/css" media="screen" />');
              $this->EE->cp->add_to_foot('<script type="text/javascript" src="' . $theme_url . 'table_operations.js"></script>');
              $this->EE->cp->add_to_foot('<script type="text/javascript" src="' . $theme_url . 'publish.js"></script>');
             */

            $this->EE->elements->_include_css($theme_url . 'styles.css', $assets);
            $this->EE->elements->_include_js($theme_url . 'table_operations.js', $assets);
            $this->EE->elements->_include_js($theme_url . 'relationship.js', $assets);
            $this->EE->elements->_include_js($theme_url . 'file.js', $assets);
            $this->EE->elements->_include_js($theme_url . 'publish.js', $assets);
            
            
            // get our site pages together
            $site_pages = $this->EE->config->item('site_pages');
            $site_id = $this->EE->config->item('site_id');
            $search_array = '';
            
            // have we got some?
            if(isset($site_pages[$site_id]['uris']))
            {
                $pages = array_merge(
                    array(
                        'https://' => 'https://',
                        'http://' => 'http://',
                    ),
                    $site_pages[$site_id]['uris']
                );
                
                // build our js list of pages for autocomplete
                foreach($pages as $page_uri)
                {
                    // remove trailing slashes if they exist, as we force them on below
                    if($page_uri[0] == '/') {
                        $page_uri = rtrim($page_uri, '/');
                    }
                    
                    $search_array .= '"'.$page_uri;
                    
                    if(substr($page_uri, -1, 1) != '/')
                    {
                        $search_array .= "/";
                    }
                    $search_array .= "\",\n";
                }
            }
            
            $search_array = rtrim($search_array, ',');
            $this->EE->cp->add_to_head('<script>var metaTableSitePages = [ '.$search_array.' ];</script>');

            $this->EE->cp->add_js_script(array('ui' => array('core', 'autocomplete')));

            // all loaded, remember it!

            $this->cache['assets_loaded'] = TRUE;
        }
        
        $col_settings = MetaTableColumn::parse_columns($this->settings["col_settings"]);
        $col_settings_n = array();
        foreach($col_settings as $settings)
        {
            $col_settings_n[] = $settings;
        }
        
        if (!$data) {
            $table_id = '__element_meta_table_index__';

            $cols = (int) $this->settings["cols"];
            $rows = (int) $this->settings["rows"];
            $header = (int) $this->settings["header"];

            $cell = array();
            for ($i = 1; $i <= ($rows * $cols); $i++) {
                $cell[] = "";
            }

            $set_focus = true;
        } else {
            if (!is_array(@unserialize($data))) {
                $data = $this->save_element($data);
            }

            $data = unserialize($data);

            $table_id = $data["table_id"];
            $cell = $data["table_data"]["cell"];
            $cols = $data["table_data"]["cols"];
            $rows = $data["table_data"]["rows"];
            $header = (int) $this->settings["header"];
            $set_focus = false;
        }

        $vars = array(
            "table_id" => $table_id,
            "cell" => $cell,
            "cols" => $cols,
            "rows" => $rows,
            "header" => $header,
            "field_name" => $this->field_name,
            "col_settings" => $col_settings_n,
        );

        return $assets.$this->EE->load->view($this->EE->elements->get_element_view_path('elements/meta_table/views/table'), $vars, TRUE);
    }

    /**
     * Parse Template (FRONTEND)
     *
     * @param string html
     * @param mixed element data
     * @param array fetched params
     * @param array element settings
     * @return string html
     */
    function replace_element_tag($data, $params = array(), $tagdata) {
        ee()->load->library('filemanager');
        
        if (!is_array(@unserialize($data))) {
            return false;
        } else {
            $data = unserialize($data);
        }
        //*
        if($this->settings['title'] == 'CTA Links' && $_GET['debug'] == '1') {
            echo '<pre>';
            echo '<b>'.$this->settings['title'].'</b>:<br/>';
            var_dump($this->settings);
            var_dump($data);
            echo '</pre>';
        }
        // */

        $col_settings = MetaTableColumn::parse_columns($this->settings["col_settings"]);
        $col_settings_n = array();
        foreach($col_settings as $settings)
        {
            $col_settings_n[] = $settings;
        }
        
        $rows = $data["table_data"]["rows"];
        $cols = $data["table_data"]["cols"];
        $cell = $data["table_data"]["cell"];

        $header = (int) $this->settings["header"];
        /*
        if ($header)
            $rows = $rows + 1;
        */
        
        //create pattern

        $table_pattern = array(
            "rows" => array(),
            "thead" => array(),
            "tbody" => array(),
            "total_rows" => $rows,
            "total_cols" => $col,
        );

        //rows

        $cell_index = 0;
        for ($i = 0; $i < $rows; $i++) {
            $table_pattern["rows"][$i] = array(
                "cells" => array(),
            );

            for ($j = 0; $j < $cols; $j++) {
                $value = !empty($cell[$cell_index]) ? $cell[$cell_index] : '';
                $cell_index++;


                //** -------------------------------
                //** replace EE entities
                //** -------------------------------    

                switch($col_settings_n[$j]->type)
                {
                    case 'file':
                        $info = explode('|', $value);
                        if(count($info) >= $this->FILE_LOC_ID) {
                            $dir = ee()->filemanager->directory($info[$this->FILE_LOC_ID], false, true);
                            $value = $dir['url'].$info[$this->FILE_NAME];
                        } else {
                            $value = '';
                        }
                        break;
                    default:
                        $value = preg_replace("/{([_a-zA-Z]*)}/u", "&#123;$1&#125;", $value);
                
                }
                
                $table_pattern["rows"][$i]['row_count'] = $i+1;
                $table_pattern["rows"][$i]["cells"][$j]["row"] = $i+1;
                $table_pattern["rows"][$i]["cells"][$j]["col"] = $j+1;
                $table_pattern["rows"][$i]["cells"][$j]["value"] = $value;
                $table_pattern["rows"][$i][$col_settings_n[$j]->name] = $value;
                $table_pattern[$col_settings_n[$j]->name.':'.$i] = $value;
            }
        }

        //thead & tbody

        foreach ($table_pattern["rows"] as $k => $row) {
            if ($k == 0 && $header) {
                $table_pattern["thead"][] = $row;
            } else {
                $table_pattern["tbody"][] = $row;
            }
        }

        $table_pattern["element_name"] = $this->element_name;

        // Put all of the template variables into a new variable returned as a handle
        $this->EE->load->library('handle_manager');
        $table_pattern["table_data"] = $this->EE->handle_manager->register($table_pattern);

        return $this->EE->elements->parse_variables($tagdata, array($table_pattern));
    }

    /**
     * Save element(BACKEND)
     *
     * @param array element data
     * @return string html
     */
    function save_element($data) {

    
        $save_data = array(
            "table_id" => $this->element_id,
            "table_data" => $data,
        );

        return serialize($save_data);
    }

    /**
     * Display settings (BACKEND)
     *
     * @param array element settings
     * @return string html
     */
    function display_element_settings($data) {
        $row_options = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            10 => 10,
            15 => 15,
        );


        $settings = array(
            array(
                lang('table_header'),
                form_dropdown('header', array(1 => $this->EE->lang->line('yes'), 0 => $this->EE->lang->line('no')), @$data['header'], 'style="width:50px"'),
            ),
            array(
                lang('table_init_rows'),
                form_dropdown('rows', $row_options, is_numeric(@$data['rows']) ? $data['rows'] : '3', 'style="width:50px"'),
            ),
            array(
                lang('table_init_cols'),
                form_input('cols', is_numeric(@$data['cols']) ? $data['cols'] : '3', 'style="width:50px"'),
            ),
            array(
                lang('table_column_settings') . '<br/>' . lang('table_column_settings_help'),
                form_textarea('col_settings', isset($data['col_settings']) ? $data['col_settings'] : ''),
            ),
        );

        return $settings;
    }

    /**
     * Preview after publish
     *
     * @param mixed element data
     * @return string html
     */
    function preview_element($data) {
        $params = array();

        $tagdata = file_get_contents(rtrim(PATH_THIRD, '/') . '/' . $this->EE->elements->addon_name . '/elements/meta_table/views/preview.php');

        return $this->replace_element_tag($data, $params, $tagdata);
    }
    
    

}

class MetaTableColumn {
    public $name;
    public $label;
    public $type = 'text';
    public $options = array();
    
    public function __construct($name, $label, $type='text', $options=array()) {
        $this->name = $name;
        $this->label = $label;
        $this->type = $type;
        $this->options = $options;
    }
    
    static function parse_columns($string)
    {
        $result = array();
        $cols = explode("\n", $string);
        foreach($cols as $col)
        {
            // Split into semicolon delimited settings:
            // name ; label
            //   OR
            // name ; label ; type
            
            // Lists and relationships have an additinal pipe and colon delimited parameter:
            // name ; label ; list ; option1:Option Label 1 | option2:Option label 2
            // name ; label ; relationship ; channel:Pages | category:55,56
            $col = explode(";", $col);
            $col_name = isset($col[0]) ? trim($col[0]) : '';
            $col_label = isset($col[1]) ? trim($col[1]) : '';
            
            // Default type should be text if it is not provided
            $col_type = isset($col[2]) ? trim($col[2]) : 'text';
            
            // Parse options if they exist
            $raw_options = isset($col[3]) ? trim($col[3]) : '';
            $col_options = array();
            if($raw_options) {
                $raw_options = explode("|", $raw_options);
                foreach($raw_options as $opt)
                {
                    $opt = explode(":", trim($opt));
                    if(count($opt) > 1) {
                        $col_options[$opt[0]] = $opt[1];
                    } else {
                        $col_options[$opt[0]] = $opt[0];
                    }
                }
            }
            $result[$col_name] = new MetaTableColumn($col_name, $col_label, $col_type, $col_options);
        }
        return $result;
    }

    public function source_name()
    {
        return implode($this->$options);
    }
}
